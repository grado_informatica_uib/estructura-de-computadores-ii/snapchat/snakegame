; =============================================================================
; COLLITIONS MANAGEMENT
; HERE YOU WILL FIND ALL SUBROUTINES AND MACROS RELATED TO THE COLLITIONS 
;==============================================================================

; -----------------------------------------------------------------------------
COLLBOARD
; COMPRUEBA COLISIONES CONTRA BORDES DEL TABLERO.
; INPUT    - D0: X COORD
;          - D1: Y COORD
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVEM.L         D0-D3,-(A7)
            MOVE.W          #CONST_OFFSET_LEFT,D2   ; LIMITE IZQUIERDO
            MOVE.W          #600,D3                 ; LIMITE DERECHO
            CMP.W           D2,D0                   
            BLT             .OUTBOUNDS              ; SI X MENOR 
            CMP.W           D0,D3
            BLT             .OUTBOUNDS              ; SI X MAYOR
            MOVE.W          #CONST_OFFSET_TOP,D2    ; LIMITE ARRIBA
            MOVE.W          #440,D3                 ; LIMITE BAJO
            CMP.W           D2,D1                   
            BLT             .OUTBOUNDS              ; SI Y MENOR QUE ARRIBA
            CMP.W           D1,D3
            BLT             .OUTBOUNDS              ; SI Y MAYOR QUE ABAJO
            BRA             .END
.OUTBOUNDS
            MOVE.W          #CONST_STAGOVR,(VAR_STANEXT)
.END        MOVEM.L         (A7)+,D0-D3
            RTS
    

; -----------------------------------------------------------------------------
COLLSNAKE
; COMPRUEBA COLISIONES CONTRA BORDES DEL TABLERO.
; INPUT    - D0: X COORD
;          - D1: Y COORD
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVEM.L         D0-D4/A0,-(A7)
            MOVE.W          (VAR_SNAKE_LONG),D2
            SUB.W           #2,D2
            LEA             VAR_SNAKE_COORD,A0
.CHECK_X    
            MOVE.W          (A0)+,D4
            CMP.W           D0,D4
            BEQ             .CHECK_Y
            MOVE.W          (A0)+,D3
            DBRA            D2,.CHECK_X
            BRA             .END
.CHECK_Y    
            MOVE.W          (A0)+,D5
            CMP.W           D1,D5
            BEQ             .EQUAL
            DBRA            D2,.CHECK_X
            BRA             .END
.EQUAL      
            MOVE.W          #CONST_STAGOVR,(VAR_STANEXT) 
.END
            MOVEM.L         (A7)+,D0-D4/A0
            RTS
  

; -----------------------------------------------------------------------------
COLLAPPLE
; COMPRUEBA COLISIONES SNAKE-APPLE.
; INPUT    - D0: X COORD
;          - D1: Y COORD
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVEM.L         D0-D4/A0,-(A7)
            MOVE.W          (VAR_SNAKE_LONG),D2
            SUB.W           #1,D2
            LEA             VAR_SNAKE_COORD,A0
.CHECK_X    
            MOVE.W          (A0)+,D4
            CMP.W           D0,D4
            BEQ             .CHECK_Y
            MOVE.W          (A0)+,D3
            DBRA            D2,.CHECK_X
            BRA             .END
.CHECK_Y    
            MOVE.W          (A0)+,D5
            CMP.W           D1,D5
            BEQ             .EQUAL
            DBRA            D2,.CHECK_X
            BRA             .END
.EQUAL      
            ADD.W           #1,(VAR_SCORE)
            JSR             SNAKEGROW
            JSR             APPLECREATE
.END
            MOVEM.L         (A7)+,D0-D4/A0
            RTS

*~Font name~Courier New~
*~Font size~14~
*~Tab type~1~
*~Tab size~4~
